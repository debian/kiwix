kiwix (2.4.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Re-upload to unstable for Python3.13 transition.
  * Replace obsolete pkg-config with pkgconf.

 -- Alexandre Detiste <tchet@debian.org>  Sun, 12 Jan 2025 13:34:42 +0100

kiwix (2.4.1-1) experimental; urgency=medium

  * New upstream version 2.4.1

 -- Kunal Mehta <legoktm@debian.org>  Sat, 21 Dec 2024 22:48:15 -0800

kiwix (2.3.1-2) unstable; urgency=medium

  * Upload to unstable

 -- Kunal Mehta <legoktm@debian.org>  Sun, 23 Jul 2023 23:17:47 -0400

kiwix (2.3.1-1) experimental; urgency=medium

  * New upstream version 2.3.1

 -- Kunal Mehta <legoktm@debian.org>  Wed, 04 Jan 2023 22:02:20 -0800

kiwix (2.3.0-1) unstable; urgency=medium

  * New upstream version 2.3.0
  * Add superficial autopkgtest

 -- Kunal Mehta <legoktm@debian.org>  Tue, 01 Nov 2022 03:17:47 -0400

kiwix (2.2.2-2) unstable; urgency=medium

  * Upload to unstable

 -- Kunal Mehta <legoktm@debian.org>  Sun, 31 Jul 2022 14:54:07 -0400

kiwix (2.2.2-1) experimental; urgency=medium

  * New upstream version 2.2.2

 -- Kunal Mehta <legoktm@debian.org>  Sat, 30 Jul 2022 20:38:11 -0400

kiwix (2.2.1-1) unstable; urgency=medium

  * New upstream version 2.2.1

 -- Kunal Mehta <legoktm@debian.org>  Wed, 06 Apr 2022 23:19:15 -0400

kiwix (2.1.0-2) unstable; urgency=medium

  * Upload to unstable

 -- Kunal Mehta <legoktm@debian.org>  Wed, 02 Feb 2022 20:27:51 -0800

kiwix (2.1.0-1) experimental; urgency=medium

  * New upstream version 2.1.0

 -- Kunal Mehta <legoktm@debian.org>  Mon, 31 Jan 2022 20:54:43 -0800

kiwix (2.0.5-3) unstable; urgency=medium

  * Add upstream patches to download catalog/resources over HTTPS

 -- Kunal Mehta <legoktm@debian.org>  Fri, 19 Feb 2021 14:30:22 -0800

kiwix (2.0.5-2) unstable; urgency=medium

  * Drop kiwix-tools dependency, no longer needed

 -- Kunal Mehta <legoktm@debian.org>  Wed, 02 Dec 2020 18:33:06 -0800

kiwix (2.0.5-1) unstable; urgency=medium

  * New upstream version 2.0.5
  * Explicitly select qt5 in d/rules

 -- Kunal Mehta <legoktm@debian.org>  Tue, 17 Nov 2020 22:44:03 -0800

kiwix (2.0.4-3) unstable; urgency=medium

  * Improve package description
  * Remove explicit build dependency on aria2

 -- Kunal Mehta <legoktm@debian.org>  Wed, 05 Aug 2020 15:15:33 -0700

kiwix (2.0.4-2) unstable; urgency=medium

  * Switch to debhelper compat 13
  * Add patch to fix upstream version

 -- Kunal Mehta <legoktm@debian.org>  Wed, 22 Jul 2020 15:40:03 -0700

kiwix (2.0.4-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol

  [ Kunal Mehta ]
  * Remove outdated gbp.conf
  * New upstream version 2.0.4
  * Refresh packaging, reintroduce to Debian (Closes: #763321)

 -- Kunal Mehta <legoktm@debian.org>  Wed, 15 Jul 2020 18:37:17 -0700

kiwix (0.9~rc2+git20140505-1) unstable; urgency=low

  [ upstream ]
  * Imported new upstream git snapshot, sha has 8faef3.
    + Hopefully Closes: bug#690418 (Upstream not quite sure).
      Thanks to mahashakti89.
    + Fixes FTBFS with GCC-4.8.
      Closes: bug#701302, Thanks to Mathias Klose.
    + Fixes FTBFS due to bug in xulrunner-dev package.
      Closes: bug#713090, Thanks to Lucas Nussbaum.

  [ Vasudev Kamath ]
  * Depends on xulrunner-24.0.
    Closes: bug#714291, Thanks to Raf Czlonka.
  * Switch packaging from short-form dh to CDBS
  * Fix home page URL to contain ending / in debian/control.
  * Bump debhelper compatibitility level to 8 to support backporting.
  * Drop kiwix.manpages all installation is handled by autotools itslef.
  * Bump Standards-Version to 3.9.5, no change needed for package source.
  * Use canonical URL for Vcs-* fields.
  * Update package relations:
    + Relax build-depend unversioned on debhelper: Needed version
      satisfied in stable.
    + Build-depend on dh-buildinfo, to improve ability to debug packaging
      independent from central Debian resources.
    + Build-depend on devscripts, to enable CDBS copyright checking.
  * Update copyright file:
    + Update Upstream-Contacts with list of contacts provided by upstream
      in their website.
    + Use Zlib as License shortname for base64.*, as recommended in
      copyright file format.
    + Fix license and copyright of several upstream source, thanks to CDBS
      copyright check feature.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Tue, 06 May 2014 11:29:08 +0530

kiwix (0.9~beta6.8-1) unstable; urgency=low

  [ Mike Gabriel ]
  * New upstream release (0.9~beta6.6).
    + Rename of base64.h by upstream (addressing closed #679605).
  * debian/patches/01_avoid_installing_headers.patch:
    + Dropped, as it is included in new upstream version.

  [ Vasudev Kamath ]
  * New upstream release (0.9~beta6.8):
    + Fixes an ugly crash related issue when using related to #679945
  * debian/control:
    + Added missing aria2 as dependency to solve an crash issue. Thanks to
      Sebastian Ramacher for pointing it out. (Closes: #679945)

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Tue, 03 Jul 2012 22:15:40 +0530

kiwix (0.9~beta6.5-1) unstable; urgency=low

  [ Vasudev Kamath ]
  * New upstream release:
    + French manpages are now installed to /usr/share/man/fr/man1
      by upstream (Closes: #677212).
  * debian/rules:
    + Removed rules for installing French manpages
  * debian/patches/01_avoid_installing_headers.patch:
    + Applied patch to avoid installing headers to /usr/include
      (Closes: #679605)

  [ Mike Gabriel ]
  * debian/watch:
    + Adapt watch file to tarball naming scheme found on upstream site
      since 0.9~beta6.
  * debian/rules:
    + Removal of aclocal.m4 is now handled by upstream Makefile.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Sat, 30 Jun 2012 20:05:17 +0530

kiwix (0.9~beta6.3-1) unstable; urgency=low

  * New upstream release:
    - static/jqueryui/js/*.js: Files have been remove by upstream as requested
      by Debian FTP master Luca Falavigna.
    - Improved clean up rules provided by upstream.
  * /debian/rules:
    + Add --enable-jar option to configure run.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Fri, 15 Jun 2012 13:17:13 +0200

kiwix (0.9~beta6-1) unstable; urgency=low

  [ Vasudev Kamath ]
  * Initial release (Closes: #617813)

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Fri, 20 Apr 2012 21:42:47 +0530
